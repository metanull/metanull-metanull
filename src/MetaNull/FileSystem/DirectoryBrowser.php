<?php

    namespace MetaNull\FileSystem;
    
    /**
      * Recursively browse a Directory
      */
class DirectoryBrowser
{
    
    /**
      * Verify if a path exists and if it is a directory
      * @return mixed Returns the absolute path to the directory if it exists, or returns FALSE otherwise
      */
    public static function isDirectory($directory)
    {
        $root = realpath($directory);
        if (false === $root) {
            return false;
        }
        if (false === is_dir($root)) {
            return false;
        }
        return $root . DIRECTORY_SEPARATOR;
    }
        
    /**
      * Browse a directory or subdirectory
      * @param string $stepInto The RELATIVE path to the directory, inside
      *     the rootDirectory
      * @param string $rootDirectory The starting point for browsing (the
      *     function will not allow brosing outside of this directory)
      * @example # Browses the /tmp directory
      *     $dir->browse('.', '/tmp');
      * @example # Browse thes /tmp/whatever directory
      *     $dir->browse('./whatever', '/tmp');
      */
    public function browse($stepInto, $rootDirectory)
    {
        // Check 1: Validate that $rootDirectory exists and is a directory
        $root = self::isDirectory($rootDirectory);
        if (false === $root) {
            throw new \Exception("Root directory not found '$rootDirectory'");
        }
        // Enter into and process the directory
        return $this->stepInto($stepInto, $root);
    }
        
    /**
      * Processes subdirectories (it is called within browse())
      * The purpose of splitting the two functions (browse and stepInto) is
      * to avoid repeating the validity check of the ROOT directory at each
      * turn of the loop
      * @param string $stepInto The RELATIVE path to the directory, inside
      *     the rootDirectory
      * @param string $rootDirectory The starting point for browsing (the
      *     function will not allow brosing outside of this directory)
      * @return int The number of sub-directories processed
      */
    protected function stepInto($stepInto, $rootDirectory)
    {
        // Check #2: Validate that DIR exists and is inside ROOT
        $absolute = self::isDirectory($rootDirectory . $stepInto);
        if (false === $absolute) {
            throw new \Exception("Directory not found '$rootDirectory$stepInto'");
        }
        if (0 !== strpos($absolute, $rootDirectory, 0)) {
            throw new \Exception("Directory '$absolute' is outside Root directory '$rootDirectory'");
        }
        // Check #3: Parse Relative path
        $relative = '';
        if ($absolute != $rootDirectory) {
            $relative = substr($absolute, strlen($rootDirectory));
        }
        // Open the directory
        $dh = opendir($absolute);
        if (false === $dh) {
            throw new \Exception("Directory '$absolute' can't be opened for browsing");
        }
        $dirCount = 1;
        try {
            // Browsing $absolute
            while (false !== ($file = readdir($dh))) {
                if ('.' == $file || '..' == $file) {
                    continue;
                }
                $file_relative = $relative . $file;
                $file_absolute = $absolute . $file;
                if (true === is_dir($file_absolute)) {
                    // File is a directory, call the Directory Handler
                    if ($this->directoryHandler($file_relative, $file_absolute)) {
                        // Directory Handler has returned true, let's step into the directory
                        $dirCount += $this->stepInto($file_relative, $rootDirectory);
                    } else {
                        // Directory Handler has returned false... let's ignore this directory
                        // > do nothing
                    }
                } else {
                    // File is not a directory, call the File Handler
                    $mimeType = mime_content_type($file_absolute);
                    $this->fileHandler($file_relative, $file_absolute, $mimeType);
                }
            }
        } finally {
            // Close the directory
            closedir($dh);
        }
        return $dirCount;
    }

    /**
     * @var callable[] $filters Stores the list of Directory Filter callbacks
     */
    protected $filters = [];
    /**
     * Add a new Directory Filter callback
     * @param callable $filter A callback accepting one single argument:
     *      function(array $args)
     * @example $dir->filter(function($param){
     *      # The $param array has two items: 'relative' and 'absolute'
     *      # Both describing the path to the directory
     *      # For the sake of the example let's keep only the tmp sub-dir.
     *      if( 0 !== strpos($args['relative'], 'tmp' )) {
     *          return false;
     *      }
     *      return true;
     *  });
     */
    public function filter(callable $filter)
    {
        $this->filters[] = $filter;
    }
        
    /**
     * Handles sub-directores
     * @param string $relative The relative path of the sub-directory
     * @param string $absolute The absolute path to th sub-directory
     * @return boolean Returns true if the subdirectory should be processed
     *  Returns false otherwise.
     */
    protected function directoryHandler($relative, $absolute)
    {
        $arg = ['relative'=>$relative,'absolute'=>$absolute];
        foreach ($this->filters as $filter) {
            if (true === $filter($arg)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @var callable[] $handlers Stores the list of File handler callbacks.
     *      The key of the array is the mime-type processed by the handler.
     */
    protected $handlers = [];
    /**
     * Add a new File Handler callback
     * @param string $mimeType The detected mime-type of the file
     * @param callable $filter A callback accepting one single argument:
     *      function(array $args)
     * @example $dir->handler('image/jpeg',function($param){
     *      # The $param array has 3 items: 'relative', 'absolute', 'mime'
     *      # For the sake of the example let's resize the image.
     *      $jpgPath = $param['absolute'];
     *      $image = Intervention\Image\ImageManagerStatic::make($jpgPath);
     *      $image->widen(320);
     *      $image->save();
     *      return true;
     *  });
     */
    public function handler($mimeType, callable $handler)
    {
        $this->handlers[$mimeType] = $handler;
    }
    /**
     * Handles files inside sub-directores
     * @param string $relative The relative path of the sub-directory
     * @param string $absolute The absolute path to th sub-directory
     * @param string $mimeType The detected mime-type of the file
     * @return boolean Returns true if the files was processed, or returns
     *      false otherwise.
     */
    protected function fileHandler($relative, $absolute, $mimeType)
    {
        $arg = ['relative'=>$relative,'absolute'=>$absolute,'mime'=>$mimeType];
        if (array_key_exists($mimeType, $this->handlers)) {
            return $this->handlers[$mimeType]($arg);
        }
        if (array_key_exists('*', $this->handlers)) {
            return $this->handlers['*']($arg);
        }
        return false;
    }
}
