<?php

    namespace MetaNull\Helper;

    /**
     * Helper classes to manipulate images
     * (Add functionalities to the intervention/image package)
     */
class ImageHelper
{
        
    /**
     * @param Intervention\Image\ImageManagerStatic & $image The image
     * @param string $watermark String to print on the image
     * @param string|array $foregroundColor Watermark's text color (HTML string '#000000' or RGBA array [0,0,0,1]  )
     * @param string|array $backgroundColor Watermark's background color (HTML string '#000000' or RGBA array [0,0,0,1]  )
     * @param string $fontFile Path to the TTF Font file
     * @param int $fontSizePx Size of the font in PX
     */
    public static function copyrightWatermark(
        &$image,
        $watermark,
        $foregroundColor = '#000000',
        $backgroundColor = '#ffffff',
        $fontFile = "C:\\Windows\\Fonts\\calibri.ttf",
        $fontSizePx = 24
    ) {
        $fontSize = $fontSizePx;
        $fontSizePt = $fontSize * .75;
            
        // Compute the text bounding box
        $bbox = imagettfbbox($fontSizePt, 0, $fontFile, $watermark);
        $bbox = [
            'top' => $bbox[5],
            'right' => $bbox[4],
            'bottom' => $bbox[1],
            'left' => $bbox[0],
        ];

        // Text Offset from the bottom-right corner of the image
        $offset = $fontSizePx;

        // Draw a tranluscent rectangle for the backougr
        $image->rectangle(
            ($image->width()-$bbox['right']) - $offset,
            ($image->height()+$bbox['top']) - $offset,
            ($image->width()) + $offset,
            ($image->height()) + $offset,
            function ($draw) use($foregroundColor, $backgroundColor){
                $draw->background($backgroundColor);
            }
        );

        // Write the watermark
        $image->text(
            $watermark,
            $image->width() - ($offset/2),
            $image->height() - ($offset/2),
            function ($font) use ($fontFile, $fontSize, $foregroundColor, $backgroundColor) {
                $font->color($foregroundColor);
                $font->file($fontFile);
                $font->size($fontSize);
                $font->align('right');
            }
        );

        // Return the image
        return $image;
    }
}
